# -*- coding: utf-8 -*-

from rest_framework import serializers
from .models import News


class NewsSerializer(serializers.ModelSerializer):

    class Meta:
        model = News
        fields = ('text', 'title', 'published', 'topic')
