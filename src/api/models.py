# -*- coding: utf-8 -*-

from django.db import models


class NewsManager(models.Manager):

    def create_news(self, text, title, published, topic):
        news = self.create(text=text, title=title, published=published, topic=topic)
        return news


class News(models.Model):
    title = models.CharField(max_length=500)
    published = models.DateTimeField(name='published')
    topic = models.CharField(max_length=1000)
    text = models.CharField(max_length=1500)

    objects = NewsManager()
