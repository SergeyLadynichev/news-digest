# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='news',
            name='hash_id',
        ),
        migrations.RemoveField(
            model_name='news',
            name='image',
        ),
        migrations.AddField(
            model_name='news',
            name='text',
            field=models.CharField(default=datetime.datetime(2017, 8, 28, 4, 18, 56, 632703, tzinfo=utc), max_length=1500),
            preserve_default=False,
        ),
    ]
