# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=500)),
                ('published', models.DateTimeField()),
                ('topic', models.CharField(max_length=1000)),
                ('image', models.CharField(max_length=2000)),
                ('hash_id', models.CharField(max_length=150)),
            ],
        ),
    ]
