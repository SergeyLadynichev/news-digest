# -*- coding: utf-8 -*-

import datetime

from rest_framework import generics

from .models import News
from .serializers import NewsSerializer


class NewsList(generics.ListCreateAPIView):
	serializer_class = NewsSerializer

	def get_queryset(self):
		"""
		:return: query. With filters (dates, topic) or objects.all()
		"""
		query = News.objects.all()

		from_param = self.check_date_param('from')
		to_param = self.check_date_param('to')

		print(from_param, to_param, self.request.query_params.get('topic'))

		#   apply date params
		if from_param or to_param:
			query = query.filter(published__range=[from_param, to_param])

		#   apply topic param
		if self.request.query_params.get('topic'):
			value = self.request.query_params.get('topic')
			query = query.filter(topic=value)

		return query

	def check_date_param(self, name):
		"""
		check exists date params in url
		:return: from or to param
		"""
		param = self.request.query_params.get(name)
		if param:
			year, month, day = param.split('-')
			param = str(datetime.datetime(int(year), int(month), int(day)).date())
		else:
			param = datetime.datetime(2000, 1, 1) if name == 'from' else datetime.datetime(2220, 1, 1)
		return param
