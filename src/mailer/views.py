# -*- coding: utf-8 -*-

import json

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from core.tasks import Mailer


@csrf_exempt
def mail(request):
	"""
	:param request: html, user mail
	:return: async sending mail to target address
	"""

	try:
		mail_params = json.loads(request.body.decode('utf-8'))
	except Exception:
		raise

	#   add celery task
	Mailer.send_mail.delay(send_to=mail_params['email_address'],  html_page=mail_params['html'])

	return HttpResponse('check your mail in few minutes')
