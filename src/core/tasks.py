# -*- coding: utf-8 -*-

from __future__ import absolute_import

import functools
import os
import smtplib
import uuid
from datetime import timedelta
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate

import feedparser
from celery.decorators import task
from celery.task import periodic_task
from dateutil.parser import parse
from weasyprint import HTML

from api.models import News


def check_duplicate(func):
	"""
	checking existing news in database
	"""

	@functools.wraps(func)
	def inner(*args, **kwargs):
		if News.objects.filter(title=(kwargs['title'])).count() == 0:
			return func(*args, **kwargs)
		else:
			pass

	return inner


def parse_feed(feed):
	"""
	:param feed: rss from lenta.ru/rss
	:return: news props
	"""
	try:
		text = feed['summary']
		title = feed['title']
		published = feed['published']
		topic = feed['tags'][0]['term']
	except KeyError:
		raise KeyError
	return text, title, parse(published), topic


@periodic_task(run_every=timedelta(seconds=10))
def get_rss():
	"""
	periodic server polling (adding news)
	"""

	print('task begins')
	# TODO refactor: move url to config
	feed = feedparser.parse('http://lenta.ru/rss')
	for item in feed['items']:
		text, title, published, topic = parse_feed(item)
		add_news(text=text, title=title, published=published, topic=topic)
	print('task finished')


@check_duplicate
def add_news(text, title, published, topic):
	"""
	save news to database
	"""
	news = News.objects.create_news(
		text=text, title=title, published=published, topic=topic)
	news.save()
	print(news)


class Mailer:

	def __init__(self): pass

	@staticmethod
	@task(name='send_mail')
	def send_mail(send_to, html_page=None):
		"""
		send email via google mail
		:param send_to: email address
		:param html_page: #container markup
		"""

		msg = MIMEMultipart()
		msg['From'] = 'digest.mailer.news@gmail.com'
		msg['To'] = send_to
		msg['Date'] = formatdate(localtime=True)
		msg['Subject'] = 'digest news'

		#   mail message
		text = 'news from lenta.ru'

		msg.attach(MIMEText(text))

		#   generate attachment
		pdf_file_path = Mailer.create_pdf(html_page=html_page)
		file_name = pdf_file_path.split('/')[2]

		with open(pdf_file_path, "rb") as fil:
			part = MIMEApplication(fil.read(), Name=msg['Subject'])
		part['Content-Disposition'] = 'attachment; filename="{}"'.format(file_name)
		msg.attach(part)

		# TODO refactor: move to config
		smtp = smtplib.SMTP('smtp.gmail.com', 587)
		smtp.starttls()
		smtp.login("digest.mailer.news@gmail.com", "newsnewsdigest")
		smtp.sendmail(msg['From'], send_to, msg.as_string())
		smtp.close()

		# remove file after mail sanded
		if os.path.isfile(pdf_file_path):
			os.remove(pdf_file_path)

	@staticmethod
	def create_pdf(html_page):
		"""
		:param html_page: table markup
		:return: pdf doc (bytes)
		"""

		file_unique_id = str(uuid.uuid4().fields[-1])[:5]

		# TODO refactor: move path to config
		pdf_file_path = "mailer/mailer_temp_folder/news_digest_{}.pdf"\
			.format(file_unique_id)

		html = HTML(string=html_page, encoding="utf-8")
		main_doc = html.render()
		pdf = main_doc.write_pdf()

		with open(pdf_file_path, 'wb') as pdf_file:
			pdf_file.write(pdf)

		return pdf_file_path
