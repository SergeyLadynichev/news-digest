# -*- coding: utf-8 -*-

from django.contrib import admin
from django.views.generic import TemplateView
from django.views.static import serve
from django.urls import path, include, re_path

from .settings import MEDIA_ROOT

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', TemplateView.as_view(template_name='index.html')),
    path('api/', include('api.urls')),
    path('mailer/', include('mailer.urls')),
    re_path(r'^assets/(?P<path>.*)$', serve, {'document_root': MEDIA_ROOT}),
]
