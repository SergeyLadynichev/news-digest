# -*- coding: utf-8 -*-

from __future__ import absolute_import

import os

from celery import Celery
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
os.environ['DJANGO_SETTINGS_MODULE'] = 'core.settings'

app = Celery('core',
             broker='amqp://admin:mypass@rabbit:5672',
             backend='rpc://',
             include=['core.tasks'])


app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
