import os
import sys

from django.core.wsgi import get_wsgi_application

path = os.getcwd()

if path not in sys.path:
    sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'core.settings'

application = get_wsgi_application()
