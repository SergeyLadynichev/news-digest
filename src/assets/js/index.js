var React = require('react')
var ReactDOM = require('react-dom')
var queryString = require('query-string');
var ReactQueryParams = require('react-query-params');
var reactStringReplace = require('react-string-replace')


var styles = {
    "marginTop": "25px;"
}


var styles_topic = {
    "fontSize": "14px;"
}


var NewsList = React.createClass({
    loadNewsFromServer: function(){

        function updateQueryStringParameter(uri, key, value) {
                var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
                var separator = uri.indexOf('?') !== -1 ? "&" : "?";
                    if (uri.match(re)) {
                        return uri.replace(re, '$1' + key + "=" + value + '$2');
                    }
                    else {
                        return uri + separator + key + "=" + value;
                    }
        }

        function getUrl(url) {
            var topic = queryString.parse(location.search)['topic']
            var from = queryString.parse(location.search)['from']
            var to = queryString.parse(location.search)['to']

            if(topic) { url = updateQueryStringParameter(url, 'topic', topic) }
            if(from) {url = updateQueryStringParameter(url, 'from', from) }
            if(to) { url = updateQueryStringParameter(url, 'to', to) }

            return url
        }

        $.ajax({
            url: getUrl(this.props.url),
            datatype: 'json',
            cache: false,
            success: function(data) {
                this.setState({data: data});
            }.bind(this)
        })
    },

    getInitialState: function() {
        return {data: []};
    },

    componentDidMount: function() {
        this.loadNewsFromServer();
        setInterval(this.loadNewsFromServer,
                    this.props.pollInterval)
    },

    render: function() {
        if (this.state.data) {
            console.log('DATA!')
            var newsNodes = this.state.data.map(function(news){
                return <li style={styles}>
                            <p>{news.title}</p>
                            <p style={styles_topic}>{news.text}</p>
                            <i><span style={styles_topic}>{news.topic}</span></i> &nbsp;

                            {reactStringReplace(news.published, /\T..+/g, (match, i) => (
                               <i><span style={styles_topic}> {match} </span></i>
                            ))}
                      </li>
            })
        }
        return (
            <div>
                <ul>
                    {newsNodes}
                </ul>
            </div>
        )
    }
})

ReactDOM.render(<NewsList url='/api/' pollInterval={1000} />,
    document.getElementById('container'))
