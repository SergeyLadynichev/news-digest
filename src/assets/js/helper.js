document.getElementById('get_report').addEventListener('click', function() {
     var email = document.getElementById('email').value;
     if(validateEmail(email)) {
     fetch("/mailer/",
        {
            method: "POST",
            body: JSON.stringify({
                'html': document.getElementById('container').innerHTML,
                'email_address': email
            })
        })
        .then(function(data){ alert('Check your mail box in next few minutes') })
     } else {
        alert('Error... Invalid email')
     }
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
}

document.getElementById('from').addEventListener('change', function() {
     window.history.pushState("object or string", "Title", updateQueryStringParameter(
                window.location.href, 'from', document.getElementById('from').value));
})

document.getElementById('to').addEventListener('change', function() {
    window.history.pushState("object or string", "Title", updateQueryStringParameter(
                window.location.href, 'to', document.getElementById('to').value));
})

document.getElementById('topics-input').addEventListener('input', function () {
        window.history.pushState("object or string", "Title", updateQueryStringParameter(
                                window.location.href, 'topic', document.getElementById('topics-input').value));

});

