Установить RabbitMQ (брокер для celery)
Сконфигурировать RabbitMQ:

sudo rabbitmqctl add_user digest digest
sudo rabbitmqctl add_vhost digest_vhost
sudo rabbitmqctl set_user_tags digest digest
sudo rabbitmqctl set_permissions -p digest_vhost digest ".*" ".*" ".*"


Запустить rabbit
rabbitmq-server start (macos: brew services start rabbitmq)

Установить Celery: pip install Celery

Установить зависимости для проекта
npm install
pip install -r requirements.txt
python manage.py collectstatic
export DJANGO_SECRET_KEY='your-secret-key'
/node_modules/.bin/webpack --config webpack.config.js


Собрать веб интерфейс (реакт компонент)
webpack assets/js/index.js assets/bundles/main.js

Запустить воркера и стартовать web сервер (удобно делать через tmux)
celery -A core worker --loglevel=info --beat
python3 manage.py runserver 127.0.0.1:8000
или зарустить скрипт RUN.sh

Интерфейс тестировался только в Google Chrome
127.0.0.1:8000


/* init.sh актуален для macos */



