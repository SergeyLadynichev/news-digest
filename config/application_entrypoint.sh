#!/bin/bash

sleep 7 # waiting for database container init
node /app/node_modules/webpack/bin/webpack.js assets/bundles/main.js
python /app/manage.py collectstatic --noinput
python /app/manage.py migrate --noinput
uwsgi --http 0.0.0.0:8000 --wsgi-file /app/core/wsgi.py
