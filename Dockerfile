FROM python:3.6

ENV PYTHONUNBUFFERED 1

RUN mkdir /app
COPY src/ config/ /app/
WORKDIR /app/

RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -
RUN apt-get install -y nodejs && npm install
RUN node node_modules/webpack/bin/webpack.js  --config webpack.config.js
RUN chmod a+x application_entrypoint.sh worker_entrypoint.sh
RUN pip install -r requirements.txt

EXPOSE 8000

